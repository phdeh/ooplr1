import java.util.*
import java.lang.Long as JLong

/**
 * @author Д. С. Воробьев, 4631
 */

/*
Вариант 3
Создать класс «BitString» для работы с 128-битовыми строками. Битовая строка должна быть представлена двумя полями типа
std::uint64_t. Должны быть реализованы все традиционные методы для работы с битами:
 Побитовое «И» (функция and);
 Побитовое «ИЛИ» (функция or);
 Исключающее «ИЛИ» (функция xor);
 Побитовое отрицание (функция not).
 Должны быть реализованы методы смещения на заданное количество бит:  Сдвиг влево (функция shiftLeft);
 Сдвиг вправо (функция shifftRight).
 */

/**
 * Иммутабильный класс для работы с 128-битовыми строками.
 */
class BitString {
    private val first: Long
    private val second: Long

    /**
     * Конструктор
     *
     * @param st Строка, содержащая число.
     * @param radix Система счисления, должна быть кратна двум.
     */
    constructor(st: String, radix: Int = 2) {
        if (radix > 36 || radix <= 1 || !radix.isLog2)
            throw IllegalArgumentException("radix = $radix")
        Character.DECIMAL_DIGIT_NUMBER
        var s2 = st.substring(maxOf(0, st.length - 64 / radix.log2), st.length)
        second = JLong.parseUnsignedLong(s2, radix)
        if (st.length > 64 / radix.log2) {
            val s1 = st.substring(0, st.length - 64 / radix.log2)
            first = JLong.parseUnsignedLong(s1, radix)
        } else
            first = 0
    }

    private constructor(first: Long, second: Long) {
        this.first = first
        this.second = second
    }

    /**
     * Возвращает содержащееся в BitString число в виде строки
     * в двоичной системе счисления.
     *
     * @return строка
     */
    override fun toString(): String = toString(2)

    /**
     * Возвращает содержащееся в BitString число в виде строки
     * в любой системе счисления, кратной двум.
     *
     * @return строка
     */
    fun toString(radix: Int): String {
        return if (radix > 36 || radix <= 1 || !radix.isLog2)
            throw IllegalArgumentException("radix = $radix")
        else if (JLong.compareUnsigned(first, 0L) == 0)
            JLong.toUnsignedString(second, radix)
        else {
            val s = JLong.toUnsignedString(second, radix)
            JLong.toUnsignedString(first, radix) + zeros(64 / radix.log2 - s.length) + s
        }
    }

    private fun getFirst() = first

    private fun getSecond() = second

    private fun zeros(number: Int): String {
        val s = StringBuilder()
        for (i in 1..number)
            s.append('0')
        return s.toString()
    }

    /**
     * Производит логическую операцию «И» для каждого
     * бита в BitString.
     *
     * @param other правый BitString в логической операции
     *
     * @return BitString
     */
    infix fun and(other: BitString): BitString {
        val f = first and other.first
        val s = second and other.second
        return BitString(f, s)
    }

    /**
     * Производит логическую операцию «ИЛИ» для каждого
     * бита в BitString.
     *
     * @param other правый BitString в логической операции
     *
     * @return BitString
     */
    infix fun or(other: BitString): BitString {
        val f = first or other.first
        val s = second or other.second
        return BitString(f, s)
    }

    /**
     * Производит логическую операцию «ИСКЛЮЧАЮЩЕЕ ИЛИ» для каждого
     * бита в BitString.
     *
     * @param other правый BitString в логической операции
     *
     * @return BitString
     */
    infix fun xor(other: BitString): BitString {
        val f = first xor other.first
        val s = second xor other.second
        return BitString(f, s)
    }

    /**
     * Производит логическую операцию «НЕ» для каждого
     * бита в BitString.
     *
     * @return BitString
     */
    fun inv(): BitString {
        val f = first.inv()
        val s = second.inv()
        return BitString(f, s)
    }

    /**
     * Производит логическую операцию «НЕ» для каждого
     * бита в BitString.
     *
     * @return BitString
     */
    operator fun not() = inv()

    /**
     * Производит логический сдвиг BitString влево.
     *
     * @param bitCount на сколько бит будет осуществлён сдвиг
     *
     * @return BitString
     */
    infix fun shl(bitCount: Int): BitString {
        val i = bitCount % 128
        val f = if (i == 0)
            first
        else if (i < 64)
            first.shl(i) or second.ushr(64 - i)
        else if (i == 64)
            second
        else
            second.shl(i)
        val s = if (i == 0)
            second
        else if (i < 64)
            second.shl(i)
        else
            0L
        return BitString(f, s)
    }

    /**
     * Производит логический сдвиг BitString влево.
     *
     * @param bitCount на сколько бит будет осуществлён сдвиг
     *
     * @return BitString
     */
    fun shiftLeft(bitCount: Int) = shl(bitCount)

    /**
     * Производит арифметический сдвиг BitString вправо.
     *
     * @param bitCount на сколько бит будет осуществлён сдвиг
     *
     * @return BitString
     */
    infix fun shr(bitCount: Int): BitString {
        val i = bitCount % 128
        val f = if (i == 0)
            first
        else if (i < 64)
            first.shr(i)
        else
            first.shr(63)
        val s = if (i == 0)
            second
        else if (i < 64)
            second.ushr(i) or first.shl(64 - i)
        else if (i == 64)
            first
        else
            first.shr(i)
        return BitString(f, s)
    }

    /**
     * Производит логический сдвиг BitString вправо.
     *
     * @param bitCount на сколько бит будет осуществлён сдвиг
     *
     * @return BitString
     */
    infix fun ushr(bitCount: Int): BitString {
        val i = bitCount % 128
        val f = if (i == 0)
            first
        else if (i < 64)
            first.ushr(i)
        else
            0L
        val s = if (i == 0)
            second
        else if (i < 64)
            second.ushr(i) or first.shl(64 - i)
        else if (i == 64)
            first
        else
            first.ushr(i)
        return BitString(f, s)
    }

    /**
     * Производит логический сдвиг BitString вправо.
     *
     * @param bitCount на сколько бит будет осуществлён сдвиг
     *
     * @return BitString
     */
    fun shiftRight(bitCount: Int) = ushr(bitCount)

    override fun equals(other: Any?) = other != null &&
            other is BitString &&
            other.first == this.first &&
            other.second == this.second

    override fun hashCode() = first.hashCode() + second.hashCode()
}

/**
 * Принадлежит ли число множеству 2^n
 *
 * @return булево значение
 */
val Int.isLog2: Boolean
    get() {
        var i = 0
        var j = 1
        while (j < this) {
            i++
            j *= 2
        }
        return this == j
    }

/**
 * Логарифм числа по основанию 2.
 *
 * @return число
 */
val Int.log2: Int
    get() {
        var i = 0
        var j = 1
        while (j < this) {
            i++
            j *= 2
        }
        return i
    }

fun main(args: Array<String>) {

}

