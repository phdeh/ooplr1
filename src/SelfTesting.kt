import java.util.*
import kotlin.concurrent.thread
import kotlin.concurrent.timerTask

fun main(args: Array<String>) {
    test()
}

fun test() {
    println("Testing.")
    for (t in Tests.values()) {
        val test = t.test
        if (test != null) {
            test(t.name)
        }
    }
}

fun test(timeout: Int = 1000, test: () -> Unit) = Test(1000, 1, test)

fun disabledTest(timeout: Int = 1000, test: () -> Unit) = Unit


data class Test(val timeout: Int, val times: Int, val test: () -> Unit) {
    operator fun invoke(str: String) {
        val timer = Timer()
        var done = false
        val thread = thread(false) {
            try {
                for (i in 1..times)
                    test()
                done = true
                println("   $str... OK")
            } catch (e: Exception) {
                System.err.println("   $str... Failed:")
                e.printStackTrace()
            } finally {
                timer.cancel()
            }
        }
        timer.schedule(timerTask {
            if (!done) {
                thread.interrupt()
                System.err.println("   $str... Failed. Time out.")
            }
            timer.cancel()
        }, timeout.toLong())
        thread.start()
    }

    fun repeat(times: Int) = Test(timeout, times, test)

    operator fun not() = null
}

infix fun Any.shouldBe(any: Any) =
        if (this != any)
            throw RuntimeException("$this != $any");
        else
            Unit

