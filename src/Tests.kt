import java.math.BigInteger

enum class Tests(val test: Test?) {
    MANUAL_CREATE_BIN(test {
        val str = ("11010010" + "11010010" + "11010010" + "11010010" +
                "11010010" + "11010010" + "11010010" + "11010010" +
                "01001100" + "01001100" + "01001100" + "01001100" +
                "01001100" + "01001100" + "01001100" + "01001100")
        val bit = BitString(str)
        bit.toString() shouldBe str
    }),
    MANUAL_CREATE_SHORT_BIN(test {
        val str = ("11010010" + "11010010" + "11010010" + "11010010" +
                "11010010" + "11010010" + "11010010")
        val bit = BitString(str)
        bit.toString() shouldBe str
    }),
    MANUAL_CREATE_QUAD(test {
        val radix = 4
        val str = ("10231133" + "10231133" + "10231133" + "10231133" +
                "10231133" + "10231133" + "10231133" + "10231133")
        val bit = BitString(str, radix)
        bit.toString(radix) shouldBe str
    }),
    MANUAL_CREATE_OCT(test {
        val radix = 8
        val str = ("14235767" + "14235767" + "14235767" + "14235767")
        val bit = BitString(str, radix)
        bit.toString(radix) shouldBe str
    }),
    MANUAL_CREATE_HEX(test {
        val radix = 16
        val str = ("14235f67" + "1423a767" + "14235f67" + "1423a767")
        val bit = BitString(str, radix)
        bit.toString(radix) shouldBe str
    }),
    RANDOM_CREATE_BIN(test { randomNumberForRadix(2) }.repeat(defaultRepeatTimes)),
    RANDOM_CREATE_QUAD(test { randomNumberForRadix(4) }.repeat(defaultRepeatTimes)),
    RANDOM_CREATE_OCT(test { randomNumberForRadix(8) }.repeat(defaultRepeatTimes)),
    RANDOM_CREATE_HEX(test { randomNumberForRadix(16) }.repeat(defaultRepeatTimes)),
    RANDOM_AND(test {
        val str1 = randomNumberForRadix(2)
        val str2 = randomNumberForRadix(2)
        val bit1 = BitString(str1)
        val bit2 = BitString(str2)
        val big1 = java.math.BigInteger(str1, 2)
        val big2 = java.math.BigInteger(str2, 2)
        val bitResult = bit1 and bit2
        val bigResult = big1.and(big2)
        val bitStr = bitResult.toString(2)
        val bigStr = bigResult.toString(2)
        bigStr shouldBe bitStr
    }.repeat(defaultRepeatTimes)),
    RANDOM_OR(test {
        val str1 = randomNumberForRadix(2)
        val str2 = randomNumberForRadix(2)
        val bit1 = BitString(str1)
        val bit2 = BitString(str2)
        val big1 = java.math.BigInteger(str1, 2)
        val big2 = java.math.BigInteger(str2, 2)
        val bitResult = bit1 or bit2
        val bigResult = big1.or(big2)
        val bitStr = bitResult.toString(2)
        val bigStr = bigResult.toString(2)
        bigStr shouldBe bitStr
    }.repeat(defaultRepeatTimes)),
    RANDOM_XOR(test {
        val str1 = randomNumberForRadix(2)
        val str2 = randomNumberForRadix(2)
        val bit1 = BitString(str1)
        val bit2 = BitString(str2)
        val big1 = java.math.BigInteger(str1, 2)
        val big2 = java.math.BigInteger(str2, 2)
        val bitResult = bit1 xor bit2
        val bigResult = big1.xor(big2)
        val bitStr = bitResult.toString(2)
        val bigStr = bigResult.toString(2)
        bigStr shouldBe bitStr
    }.repeat(defaultRepeatTimes)),
    RANDOM_SHL(test {
        val str = randomNumberForRadix(2)
        val bitCount = random.nextInt(127)
        val bit = BitString(str)
        val big = java.math.BigInteger(str, 2)
        val bitResult = bit shl bitCount
        val bigResult = big.shiftLeft(bitCount).and(mask)
        val bitStr = bitResult.toString(2)
        val bigStr = bigResult.toString(2)
        bigStr shouldBe bitStr
    }.repeat(defaultRepeatTimes)),
    RANDOM_USHR(test {
        val str = randomNumberForRadix(2)
        val bitCount = random.nextInt(127)
        val bit = BitString(str)
        val big = java.math.BigInteger(str, 2)
        val bitResult = bit ushr bitCount
        val bigResult = big.shiftRight(bitCount).and(mask)
        val bitStr = bitResult.toString(2)
        val bigStr = bigResult.toString(2)
        bigStr shouldBe bitStr
    }.repeat(defaultRepeatTimes))
}

val defaultRepeatTimes = 1000

val random = java.util.Random()

val mask = BigInteger(
        "ffffffff" + "ffffffff" + "ffffffff" + "ffffffff", 16
)

fun randomTestForRadix(radix: Int) {
    val str = randomNumberForRadix(radix)
    val bit = BitString(str, radix)
    bit.toString(radix) shouldBe str
}

fun randomNumberForRadix(radix: Int): String {
    val digits = "0123456789abcdef".toCharArray()
    val sb = StringBuilder()
    for (i in 1..128 / radix.log2)
        sb.append(digits[random.nextInt(radix)])
    return sb.toString().replace(Regex("^0*"), "")
}